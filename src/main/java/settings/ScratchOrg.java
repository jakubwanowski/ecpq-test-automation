package settings;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import com.google.gson.Gson;

public class ScratchOrg {
	
	private BufferedReader input;
	private Process process;
	private Runtime runtime = Runtime.getRuntime();
	private String selectedDriver = Config.selectedDriver;
	private String line = null;
	private int exitValue;
	
	public static String resultJson = null;
	
	public class jsonResult {
		public String accessToken;
		public String instanceUrl;
	}
	
	public class jsonBody {
		public Integer status;
		public jsonResult result;
	}
	
	public ScratchOrg() {
		//loginToDevHub();
		//scratchOrgCreation();
		//deployChanges();
		//assignPermissionSet();
		openScratchOrg();
	}
	
	public void loginToDevHub() {
		
		System.out.println("- LOGIN TO DEVHUB - START");
		
		try {
			// SFDX script
			process = runtime.exec("");
			
			input = new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			while ((line = input.readLine()) != null ) {
				System.out.println(" " + line);
			}
			
			exitValue = process.waitFor();
			
			System.out.println("- LOGIN TO DEVHUB - END [" + exitValue + "]");
            
            if(exitValue != 0) {
                System.out.println("************ TEST COMPLETED [FAILURE] ************");    
                return;
            }
		} 
		
		catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		
	}
	
	public void scratchOrgCreation() {
		
		System.out.println("- SCRATCH ORG CREATION - START");
		
		try {
			
			if (selectedDriver.contains("dell")) {
				process = runtime.exec("C:\\Program Files\\sfdx\\bin\\sfdx "
						+ "force:org:create -s -f config\\project-scratch-def.json -a testAutomationOrg --json", 
						null, new File("C:\\Users\\Jakub Wanowski\\Documents\\E-Commerce CPQ\\test-automation-org"));		
			} else {
				process = runtime.exec("sfdx force:org:create -s -f ../config/project-scratch-def.json -a testAutomationOrg --json");
			}
			
			
			input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            
            line=null;
            while ((line = input.readLine()) != null)
                System.out.println("  " + line);
            
            exitValue = process.waitFor();
            
            System.out.println("- SCRATCH ORG CREATION - END [" + exitValue + "]");
            
            if(exitValue != 0) {
                System.out.println("************ TEST COMPLETED [FAILURE] ************");                
                return;
            }
		} 
		
		catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}
	
	public void deployChanges() {
		
		System.out.println("- CHANGES DEPLOYMENT - START");
		
		try {
			
			if (selectedDriver.contains("dell")) {
				process = runtime.exec("C:\\Program Files\\sfdx\\bin\\sfdx force:source:push",
						null, new File("C:\\C:\\Users\\Jakub Wanowski\\Documents\\E-Commerce CPQ\\test-automation-org"));
			} else {
				process = runtime.exec("sfdx force:source:push");
			}
			
			input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            
            line=null;
            while ((line = input.readLine()) != null)
                System.out.println("  " + line);
            
            exitValue = process.waitFor();
            
            System.out.println("- CHANGES DEPLOYMENT - END [" + exitValue + "]");
            
            if(exitValue != 0) {
                System.out.println("************ TEST COMPLETED [FAILURE] ************");                 
                return;
            }
		}
		
		catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}
	
	public void assignPermissionSet() {
		
		System.out.println("- PERMISSION SET ASSIGNMENT - START");
		
		try {
			
			if (selectedDriver.contains("dell")) {
				process = runtime.exec("C:\\Program Files\\sfdx\\bin\\sfdx force:user:permset:assign -n CPQ_Administrator",
						null, new File("C:\\C:\\Users\\Jakub Wanowski\\Documents\\E-Commerce CPQ\\test-automation-org"));
				process = runtime.exec("C:\\Program Files\\sfdx\\bin\\sfdx force:user:permset:assign -n CPQ_User",
						null, new File("C:\\C:\\Users\\Jakub Wanowski\\Documents\\E-Commerce CPQ\\test-automation-org"));
			} else {
				process = runtime.exec("sfdx force:user:permset:assign -n CPQ_Administrator");
				process = runtime.exec("sfdx force:user:permset:assign -n CPQ_User");
			}
		}
		
		catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}
	
	public void openScratchOrg() {
		
		System.out.println("- OPENING SCRATCH ORG - START");
		
		try {
			
			if (selectedDriver.contains("dell")) {
				process = runtime.exec("C:\\Program Files\\sfdx\\bin\\sfdx force:org:display --json",
						null, new File("C:\\C:\\Users\\Jakub Wanowski\\Documents\\E-Commerce CPQ\\test-automation-org"));
			} else {
				process = runtime.exec("sfdx force:org:display --json");
			}
			
			input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            
            line = null;
            while ((line = input.readLine()) != null) {
                System.out.println("  " + line);
                resultJson += line;
            }
            
            exitValue = process.waitFor();

            System.out.println("- OPENING SCRATCH ORG -END [" + exitValue + "]");
            
            if(exitValue != 0) {
                System.out.println("************ TEST COMPLETED [FAILURE] ************");              
                return;
            }
		}
		
		catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		
	}
	
	public static String loginToScratchOrgJson() {
		
		Gson gson = new Gson();
		jsonBody objFromJson = gson.fromJson(resultJson, jsonBody.class);
		System.out.println("  Scratch org URL is: " + objFromJson.result.instanceUrl + "/secur/frontdoor.jsp?sid=" + objFromJson.result.accessToken);
        return objFromJson.result.instanceUrl + "/secur/frontdoor.jsp?sid=" + objFromJson.result.accessToken;
	}

}




















