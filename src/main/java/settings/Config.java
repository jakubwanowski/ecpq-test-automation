package settings;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeSuite;

import pages.ChoosePricebook;
import pages.DeveloperConsole;
import pages.Edit_Account;
import pages.Edit_Attribute;
import pages.Edit_AttributeValue;
import pages.Edit_Opportunity;
import pages.Edit_Product;
import pages.HomePage;
import pages.Home_Accounts;
import pages.Home_Attributes;
import pages.Home_Opportunities;
import pages.Home_PriceRules;
import pages.Home_Products;
import pages.LoginPage;
import pages.New_PriceRule;
import pages.New_Product;
import pages.Object_Account;
import pages.Object_Attribute;
import pages.Object_Opportunity;
import pages.Object_Product;
import pages.React_PricebookManagementTool;
import pages.React_ProductDesignPage;
import pages.React_QuoteConfigurator;
import pages.VisualForce_PriceRuleConfigurator;

public class Config {
	
	public static String environment = "scratch";
	public static String selectedDriver = "scratchFirefox";
	
	//------------------------------------------------------------------------------------------------------------------
	//	DRIVERS PATHS
	//------------------------------------------------------------------------------------------------------------------
	//Local DELL paths
	public final String geckoDriverLocalPath = "C:\\Users\\Jakub Wanowski\\Selenium\\Geckodriver\\geckodriver.exe";
	public final String chromeDriverLocalPath = "C:\\Users\\Jakub Wanowski\\Selenium\\Chromedriver\\chromedriver.exe";
	public final String nightlyPath = "C:\\Program Files\\Nightly\\firefox.exe";
	public final String firefoxPath = "C:\\Program Files\\Mozilla Firefox\\firefox.exe";
	
	//Local MAC path
	public final String chromeDriverMacLocalPath = "/Users/Waniu/IT/Selenium/chromedriver";

	//Scratch paths
	public final String geckoDriverScratchPath = "/opt/sel_driver/geckodriver";
	
	
	//------------------------------------------------------------------------------------------------------------------
	//	ORG PATHS
	//------------------------------------------------------------------------------------------------------------------
	//Test
	public final String logninTest = "jakub.wanowski@ecommcpq.force.com.test";
	public final String passwordTest = "salesforceId2010!!";
	public final String pathTest = "https://ecomms-cpq--test.cs88.my.salesforce.com";

	//Demo
	public final String logninDemo = "jakub.wanowski@ecommcpq.force.com";
	public final String passwordDemo = "salesforceId2010!";
	public final String pathDemo = "https://login.salesforce.com/";
	
	//	WEBDRIVER OBJECTS
	public static WebDriver driver;
	public static WebDriverWait wait;
	public static WebDriverWait wait60;
	
	//	PAGES OBJECTS
	public ChoosePricebook choosePri;
	public DeveloperConsole console;
	public Edit_Account editAcc;
	public Edit_Attribute editAtr;
	public Edit_AttributeValue editAtrVal;
	public Edit_Opportunity editOpp;
	public Edit_Product editProduct;
	public Home_Accounts homeAcc;
	public Home_Attributes homeAtr;
	public Home_Opportunities homeOpp;
	public Home_PriceRules homePriRuls;
	public Home_Products homePrd;
	public HomePage home;
	public LoginPage login;
	public New_PriceRule newPriRule;
	public New_Product newProduct;
	public Object_Account acc;
	public Object_Attribute atr;
	public Object_Opportunity opp;
	public Object_Product product;
	public React_PricebookManagementTool pricebookTool;
	public React_ProductDesignPage productDesign;
	public React_QuoteConfigurator quoteConfigurator;
	public VisualForce_PriceRuleConfigurator priRulsConfig;

	
	//------------------------------------------------------------------------------------------------------------------
	//	TEST DATA
	//------------------------------------------------------------------------------------------------------------------
	public final String accountName = "autoAcc";
	public final String attributeName = "autoAtr";
	public final String attributeType = "Picklist";
	public final String attributeVal1 = "Option 1";
	public final String attributeVal2 = "Option 2";
	public final String productName = "autoPrd";
	public final String productRecordType = "Product";
	public final String masterPricebookName = "autoMasterPricebook";
	public final String masterValidFromDate = "10/02/2017";
	public final String masterValidToDate = "10/02/2018";
	public final String productListPrice = "0";
	public final String option1ListPrice = "100";
	public final String option2tListPrice = "200";
	public final String partnerPricebookName = "autoPartnerPricebook";
	public final String productPriceModifier = "10";
	public final String option1PriceModifier = "100";
	public final String option2tPriceOveride = "200";
	public final String priceRuleName = "autoPriceRule";
	public final String adjustTargetValue = "3";
	public final String opportunityName = "autoOpp";
	public final String clientFirstName = "autoClientFirstName";
	public final String clientLastName = "autoClientLastName";
	public final String clientEmail = "autoClientEmail@test.com";
	
	
	//------------------------------------------------------------------------------------------------------------------
	//	DRIVER OPTIONS
	//------------------------------------------------------------------------------------------------------------------
	FirefoxOptions useFirefox = new FirefoxOptions().setBinary(firefoxPath);
	FirefoxOptions useNightly = new FirefoxOptions().setBinary(nightlyPath);
	
	
	@BeforeSuite()
	public void beforeSuite() throws InterruptedException {
		
		switch (selectedDriver) {
			case "dellChrome"    : System.setProperty("webdriver.chrome.driver", chromeDriverLocalPath);
								   driver = new ChromeDriver();
								   break;
			case "dellFirefox"   : System.setProperty("webdriver.gecko.driver", geckoDriverLocalPath);
								   driver = new FirefoxDriver(useFirefox);
								   break;
			case "dellNightly"   : System.setProperty("webdriver.gecko.driver", geckoDriverLocalPath);
			 					   driver = new FirefoxDriver(useNightly);
			 					   break;
			case "macChrome"  	 : System.setProperty("webdriver.chrome.driver", chromeDriverMacLocalPath);
			 					   driver = new ChromeDriver();
			 					   break;
			case "scratchFirefox" : System.setProperty("webdriver.gecko.driver", geckoDriverScratchPath);
								   driver = new FirefoxDriver();
								   break;
		}

		driver.manage().window().maximize();
		login = new LoginPage(driver);
		
		switch (environment) {
		case "test"    : driver.get(pathTest);
						 login.setUserName(logninTest);
						 login.setPassword(passwordTest);
						 login.clickLogin();
						 break;
		case "scratch" : ScratchOrg testOrg = new ScratchOrg();
						 driver.get(ScratchOrg.loginToScratchOrgJson());
		}
		
		Thread.sleep(7000);
	}
	
	/*@AfterSuite
	public void tearDown() {
		if (driver != null) {
			driver.close();
			driver.quit();
		}
	}*/

}
