package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChoosePricebook {

	//##########################################################################
	//	NESCESSARY OBJECTS
	//##########################################################################
	
	WebDriver driver;
	WebDriverWait wait;

	
	//##########################################################################
	//	PAGE CONSTRUCTOR
	//##########################################################################
		
	public ChoosePricebook(WebDriver driver) {
		this.driver = driver;
			
		PageFactory.initElements(driver, this);
	}
	
	
	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
	
	/*
	 *	BUTTONS
	 */
	@FindBy(name = "save")
	WebElement saveButton;
	
	
	/*
	 *	PICKLISTS
	 */
	@FindBy(id = "p1")
	WebElement pricebookPicklist;
	
	
	//##########################################################################
	// 	PAGE ACTIONS
	//##########################################################################

	public void clickSavebutton(WebDriverWait wait) {
		this.wait = wait;
		
		saveButton.click();
		
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
	
	public void selectPartnerPricebook(String partnerPricebookName) {
		Select partnerPricebook = new Select(pricebookPicklist);
		partnerPricebook.selectByVisibleText(partnerPricebookName);
	}
	
}
