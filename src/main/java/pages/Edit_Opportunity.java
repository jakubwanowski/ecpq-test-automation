package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Edit_Opportunity {

	//##########################################################################
	//	NESCESSARY OBJECTS
	//##########################################################################
	
	WebDriver driver;
	WebDriverWait wait;
	
	
	//##########################################################################
	//	PAGE CONSTRUCTOR
	//##########################################################################
		
	public Edit_Opportunity(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	
	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
	
	/*
	 * 	BUTTONS
	 */
	@FindBy(name = "save")
	WebElement saveButton;
	
	@FindBy(name = "save_new")
	WebElement saveAndAddPriceButton;
		
	@FindBy(name = "cancel")
	WebElement cancelButton;
		
	
	/*
	 * 	FIELDS
	 */
	@FindBy(id = "opp3")
	WebElement opportunityNameField;
	
	@FindBy(id = "opp9")
	WebElement closeDateField;
	@FindBy(xpath = "/html/body/div[1]/div[2]/table/tbody/tr/td[2]/form/div/div[2]/div[3]/table/tbody/tr[1]/td[4]/div/span/span/a")
	WebElement closeDateTodaysDateIcon;
	
	
	/*
	 * 	PICKLISTS
	 */
	@FindBy(id = "opp11")
	WebElement stagePicklist;
	
	
	/*
	 * 	LOOKUPS
	 */
	@FindBy(id = "opp4")
	WebElement accountNameLookupField;
	@FindBy(id = "opp4_lkwgt")
	WebElement accountNameLookupIcon;
	
	@FindBy(id = "CF00N9E000000Tw9z")
	WebElement masterPricebookLookupField;
	@FindBy(xpath = "/html/body/div[1]/div[2]/table/tbody/tr/td[2]/form/div/div[2]/div[3]/table/tbody/tr[5]/td[4]/span/a/img")
	WebElement masterPricebookLookupIcon;
	
	
	//##########################################################################
	//	PAGE ACTIONS	
	//##########################################################################
		
	public void clickSavebutton() {
		saveButton.click();
	}
	
	//	Set something
	//--------------------------------------------------------------------------
	public void setOpportunityName(String name) {
		opportunityNameField.sendKeys(name);
	}
	
	public void setAccountName(String name) {
		accountNameLookupField.sendKeys(name);
	}
	
	public void setCloseDate() {
		closeDateTodaysDateIcon.click();
	}
	
	public void setMasterPricebook(String name) {
		masterPricebookLookupField.sendKeys(name);
	}
	
	public void selectStage() {
		Select stage = new Select(stagePicklist);
		stage.selectByVisibleText("Prospecting");
	}

}
