package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

	WebDriver driver;
	WebDriverWait wait;
	
	//##########################################################################
	//	WebElements
	//##########################################################################
		
	/*
		Tab Bar Buttons
	 */
	@FindBy(linkText = "Home")
	WebElement homeButton;
	
	@FindBy(linkText = "Accounts")
	WebElement accountsButton;
	
	@FindBy(linkText = "Opportunities")
	WebElement opportunitiesButton;
	
	@FindBy(linkText = "Quotes")
	WebElement quotesButton;
	
	@FindBy(linkText = "Categories")
	WebElement categoriesButton;
	
	@FindBy(id = "Product2_Tab")
	WebElement productsButton;
	
	@FindBy(id = "01r9E0000004NQJ_Tab")
	WebElement attributesButton;
	
	@FindBy(id = "01r9E0000004NQQ_Tab")
	WebElement pricebookManagementToolButton;
	
	@FindBy(css = ".listRelatedObject.Custom67Block.title")
	WebElement priceRulesButton;
	
	@FindBy(id = "AllTab_Tab")
	WebElement allTabsButton;
	
	//--------------------------------------------------------------------------

	/*
		Others
	 */
	@FindBy(className = "msgContent")
	WebElement environmentName;
	
	@FindBy(id = "userNavLabel")
	WebElement userNavigationMenu;
	
	@FindBy(css = ".debugLogLink.menuButtonMenuLink")
	WebElement developerConsoleButton;
	
	
	//##########################################################################
	//	Page constructor
	//##########################################################################
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	//##########################################################################
	//	Page actions
	//##########################################################################
	
	public String getEnvironmentName() {
		return environmentName.getText();
	}
	
	//Click tab bar buttons
	public void clickHomeButton(WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(homeButton));
		homeButton.click();
	}
	
	public void clickAccountsButton(WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(accountsButton));		
		accountsButton.click();
	}	
	
	public void clickOpportunitiesButton(WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.elementToBeClickable(opportunitiesButton));
		wait.until(ExpectedConditions.visibilityOf(opportunitiesButton));		
		opportunitiesButton.click();
	}	
	
	public void clickQuotesButton(WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(quotesButton));		
		quotesButton.click();
	}	
	
	public void clickCategoriesButton(WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(categoriesButton));		
		categoriesButton.click();
	}	
	
	public void clickProductsButton(WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(productsButton));		
		productsButton.click();
	}
	
	public void clickAttributesButton(WebDriverWait wait) throws InterruptedException {
		this.wait = wait;
		
		String pageURL = driver.getCurrentUrl();
		
		if(pageURL.contains("home")) {
			Thread.sleep(10000);
		}
		
		wait.until(ExpectedConditions.visibilityOf(attributesButton));		
		attributesButton.click();
	}
	
	public void clickPricebookManagementToolButton(WebDriverWait wait) throws InterruptedException {
		this.wait = wait;
		
		Thread.sleep(1000);
		
		wait.until(ExpectedConditions.visibilityOf(pricebookManagementToolButton));		
		pricebookManagementToolButton.click();
	}
	
	public void clickPriceRulesButton() throws InterruptedException {
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", priceRulesButton);
		Thread.sleep(2000);
		
		priceRulesButton.click();
	}
	
	public void clickAllTabsButton(WebDriverWait wait) throws InterruptedException {
		this.wait = wait;
		
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", allTabsButton);
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.visibilityOf(allTabsButton));
		allTabsButton.click();
	}
	
	/*
	 * 	Navigation actions
	 */
	
	public void expandUserNavigationMenu() {
		userNavigationMenu.click();
	}
	
	public void openDeveloperConsole(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(developerConsoleButton));
		developerConsoleButton.click();
	}
	
	
	
	
	
}
