package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class New_Product {

	WebDriver driver;
	WebDriverWait wait;
	
	//##########################################################################
	//	Page Elements
	//##########################################################################
	
	//	Buttons
	@FindBy(xpath = "//*[@id=\"bottomButtonRow\"]/input[1]")
	WebElement continueButton;
	
	@FindBy(name = "cancel")
	WebElement cancelButton;
	
	//--------------------------------------------------------------------------
	
	//	Picklists
	@FindBy(id = "p3")
	WebElement productRecordTypePicklist;
	
	
	//##########################################################################	
	//	Page constructor
	//##########################################################################	
	
	public New_Product(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}

	//##########################################################################
	// Page actions
	//##########################################################################
	
	public void waitForPageToLoad() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(productRecordTypePicklist));
	}
	
	public void clickContinueButton() {
		continueButton.click();
	}
	
	public void clickCancelButton() {
		cancelButton.click();
	}
	
	public void selectRecordType(String recordType) {
		Select recordTypePicklist = new Select(productRecordTypePicklist);
		recordTypePicklist.selectByVisibleText(recordType);
	}
	
	public void waitForPageToLoad(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(productRecordTypePicklist));
	}
	
}
