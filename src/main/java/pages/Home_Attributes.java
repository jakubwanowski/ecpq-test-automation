package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Home_Attributes {

	//##########################################################################
	//	NESCESSARY OBJECTS
	//##########################################################################
	
	WebDriver driver;
	
	
	//##########################################################################
	//	PAGE CONSTRUCTOR
	//##########################################################################
	
	public Home_Attributes(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	
	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
	
	/*
	 * 	BUTTONS
	 */
	@FindBy(name = "new")
	WebElement newButton;

	
	//##########################################################################
	// PAGE ACTIONS
	//##########################################################################
	
	public void clickNewButton() {
		newButton.click();
	}
}
