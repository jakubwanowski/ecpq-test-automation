package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Home_Products {

	WebDriver driver;
	
	//##########################################################################
	//	Page elements
	//##########################################################################
	
	/*
		Buttons
	 */
	@FindBy(name = "new")
	WebElement newButton;
	
	
	//##########################################################################
	//	Page constructor
	//##########################################################################
	
	public Home_Products(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	//##########################################################################
	// Page actions
	//##########################################################################
	
	public void clickNewButton() {
		newButton.click();
	}
}
