package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Edit_Attribute {

	//##########################################################################
	//	NESCESSARY OBJECTS
	//##########################################################################
	
	WebDriver driver;
	WebDriverWait wait;

	
	//##########################################################################
	//	PAGE CONSTRUCTOR
	//##########################################################################
		
	public Edit_Attribute(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	
	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
	
	/*
	 * 	BUTTONS
	 */
	@FindBy(name = "save")
	WebElement saveButton;
	
	@FindBy(name = "save_new")
	WebElement saveAndNewButton;
	
	@FindBy(name = "cancel")
	WebElement cancelButton;
	
	
	/*
	 * 	FIELDS
	 */
	@FindBy(id = "Name")
	WebElement attributeNameField;
	
	
	/*
	 * 	PICKLISTS
	 */
	@FindBy(id = "00N9E000000Tw9Q")
	WebElement attributeTypePicklist;
	
		
	//##########################################################################
	// PAGE ACTIONS
	//##########################################################################
	
	public void clickSaveButton() {
		saveButton.click();
	}
	
	public void setAttributeName(String name) {
		attributeNameField.sendKeys(name);
	}
	
	public void selectAttributetype(String type) {
		Select typePicklist = new Select(attributeTypePicklist);
		typePicklist.selectByValue(type);
	}
	
	public void waitForPageToLoad(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(attributeNameField));
	}
}
