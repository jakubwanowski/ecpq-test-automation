package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Object_Account {
	
	WebDriver driver;

	//##########################################################################
	//	Page Elements
	//##########################################################################
	
	/*
		Buttons
	*/
	@FindBy(name = "edit")
	WebElement editButton;
	
	@FindBy(name = "del")
	WebElement deleteButton;
	
	@FindBy(name = "share")
	WebElement sharingButton;
	
	//--------------------------------------------------------------------------
	
	/*
		Other
	 */
	@FindBy(id = "acc2_ileinner")
	WebElement accountNameInfo;
	
	//##########################################################################
	//	Page constructor
	//##########################################################################
		
	public Object_Account(WebDriver driver) {
		this.driver = driver;
			
		PageFactory.initElements(driver, this);
	}
	
	//##########################################################################
	// Page actions
	//##########################################################################

	public String getAccountNameInfo() {
		return accountNameInfo.getText();
	}
}
