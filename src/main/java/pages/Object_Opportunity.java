package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Object_Opportunity {

	WebDriver driver;
	WebDriverWait wait;

	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
	
	/*
	 *	BUTTONS
	*/
	@FindBy(name = "edit")
	WebElement editButton;
	
	@FindBy(name = "del")
	WebElement deleteButton;
	
	@FindBy(name = "clone")
	WebElement cloneButton;
	
	@FindBy(name = "share")
	WebElement sharingButton;
	
	//--------------------------------------------------------------------------
	
	
	/*
	 *	RELATED LISTS ELEMENTS 
	 */
	
	//	QUOTES RELATED LIST
	@FindBy(xpath = "/html/body/div[1]/div[2]/table/tbody/tr/td[2]/div[5]/div[1]/div/div[2]/table/tbody/tr[2]/td[8]/a")
	WebElement linkToConfigurator;
	
	@FindBy(css = ".listRelatedObject.quoteBlock")
	WebElement quotesRelatedListBlock;
	
	//	PRODUCTS RELATED LIST
	@FindBy(name = "choosePB")
	WebElement choosePricebookButton;
	
	@FindBy(css = ".listRelatedObject.opportunityLineItemBlock")
	WebElement productsRelatedListBlock;
	
	@FindBy(xpath = "/html/body/div[1]/div[2]/table/tbody/tr/td[2]/div[6]/div[1]/div/div[1]/table/tbody/tr/td[1]")
	WebElement productRelatedListLabel;
	
	//--------------------------------------------------------------------------
	
	
	/*
	 *	OTHERS
	 */
	@FindBy(id = "Name_ileinner")
	WebElement attributeNameInfo;
	
	@FindBy(id = "00N9E000000Tw9Q_ileinner")
	WebElement attributeTypeInfo;
	
	//##########################################################################
	//	PAGE CONSTRUCTOR
	//##########################################################################
		
	public Object_Opportunity(WebDriver driver) {
		this.driver = driver;
			
		PageFactory.initElements(driver, this);
	}
	
	//##########################################################################
	// PAGE ACTIONS
	//##########################################################################

	public void clickChoosePricebook() {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", productsRelatedListBlock);
		choosePricebookButton.click();
	}
	
	public void clickConfigureLink() {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", quotesRelatedListBlock);
		linkToConfigurator.click();
	}
	
	public boolean checkProductPricebook(String pricebookName) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", productsRelatedListBlock);
		
		String productsLabel = productRelatedListLabel.getText();
		
		if(productsLabel.contains(pricebookName)) {
			return true;
		}
		else {
			return false;
		}
	}
	
}
