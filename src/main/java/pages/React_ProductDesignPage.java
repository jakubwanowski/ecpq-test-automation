package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class React_ProductDesignPage {

	WebDriver driver;
	WebDriverWait wait;

	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
	
	
	/*
		BUTTONS
	*/
	@FindBy(id = "j_id0:mainForm:j_id20")
	WebElement returnToProductButton;
	
	@FindBy(id = "j_id0:mainForm:j_id22")
	WebElement quickSaveButton;
	
	@FindBy(id = "j_id0:mainForm:j_id24")
	WebElement saveAndReturnButton;
	
	//--------------------------------------------------------------------------
	
	
	/*
		TABS
	*/
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/ul/li[1]/a")
	WebElement productAttributesTab;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/ul/li[2]/a")
	WebElement productParametersTab;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/ul/li[3]/a")
	WebElement attributeDefaultValuesTab;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/ul/li[4]/a")
	WebElement attributeValueDependenciesTab;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/ul/li[5]/a")
	WebElement savedOptionsTab;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/ul/li[7]/a")
	WebElement imagesTab;	
	
	//--------------------------------------------------------------------------	
	
	
	/*
		PRODUCT ATTRIBUTE TAB ELEMENTS
	*/
	
	@FindBy(id = "tab_productAttributes")
	WebElement tabProductAttributesInfo;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[1]/div/div[1]/h1")
	WebElement designedProductNameInfo;
	
	@FindBy(id = "attributeSets-section-header")
	WebElement expandAttributeSetButton;
	
	@FindBy(id = "attributes-section-header")
	WebElement expandAttributesButton;
	
	@FindBy(id = "attributeSearchbar")
	WebElement attributeSearchBar;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div[1]/span/div[2]/div/div[2]/span/div[2]/div/div[2]/span/div/table/tbody/tr/td[1]")
	WebElement attributeToAdd;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div[1]/span/div[2]/div/div[2]/span/div[2]/div/div[2]/span/div/table/tbody/tr[1]/td[4]")
	WebElement visibleAddAttributeIcon;
	
	@FindBy(xpath = "//*[@id=\"j_id0:mainForm:productAttributes-table\"]/div/table/tbody/tr")
	WebElement addedAttributesTable;
	
	@FindBy(xpath = "//*[@id=\"j_id0:mainForm:productAttributes-table\"]/div/table/tbody/tr/td[3]/div/span")
	WebElement optionAffectingCheckbox;
	
	//--------------------------------------------------------------------------	
	
	/*
		PRODUCT PARAMETERS TAB ELEMENTS
	 */
	@FindBy(id = "tab_productParameters")
	WebElement tabProductParametersInfo;
	
	//--------------------------------------------------------------------------	
	
	/*
		ATTRIBUTE DEFAULT VALUES TAB ELEMENTS
	 */
	@FindBy(id = "tab_AttributeDefaultValues")
	WebElement tabAttributeDefaultValuesInfo;
	
	//--------------------------------------------------------------------------	
	
	/*
		ATTRIBUTE VALUE DEPENDENCIES TAB ELEMENTS
	 */
	@FindBy(id = "tab_AttributeValueDependencies")
	WebElement tabAttributeValueDependenciesInfo;
	
	//--------------------------------------------------------------------------	
	
	/*
		PRODUCT OPTIONS TAB ELEMENTS
	 */
	@FindBy(id = "tab_productOptions")
	WebElement tabOptionsInfo;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div[5]/div[2]/a/button")
	WebElement generateOptionsButton;
	
	@FindBy(id = "j_id0:mainForm:generatedOptions-table")
	WebElement generatedOptionsTabel;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div[5]/div[3]/div/div[1]/div/div/div[1]/div/div[2]")
	WebElement addAllOptionsButton;
	
	@FindBy(id = "j_id0:mainForm:savedOptions-table")
	WebElement savedOptionsTable;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div[5]/div[3]/div/div[1]/div/div/span/table/tbody")
	WebElement availableOptionsTable;
	
	//--------------------------------------------------------------------------	
	
	
	/*
		IMAGES TAB ELEMENTS
	 */
	@FindBy(id = "tab_images")
	WebElement tabImagesInfo;
	
	
	//##########################################################################
	//	PAGE CONSTRUCTOR
	//##########################################################################
	
	
	public React_ProductDesignPage(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	
	//##########################################################################
	// PAGE ACTIONS
	//##########################################################################
	
	
	public String getDesinedProductNameInfo() {
		return designedProductNameInfo.getText();
	}
	
	public void clickReturnToProduct() {
		returnToProductButton.click();
	}
	
	public void clickQuickSave(WebDriverWait wait) throws InterruptedException {
		this.wait = wait;
		
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", quickSaveButton);
		wait.until(ExpectedConditions.visibilityOf(quickSaveButton));
		wait.until(ExpectedConditions.elementToBeClickable(quickSaveButton));
		quickSaveButton.click();
		
		Thread.sleep(1000);
		//quickSaveButton.click();
	}
	
	public void clickSaveAndReturn() {
		saveAndReturnButton.click();
	}
	
	public void waitForPageToLoad(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(designedProductNameInfo));
	}
	
	
	
	//--------------------------------------------------------------------------
	
	
	/*
		PRODUCT ATTRIBUTE TAB ACTIONS
	*/
	public void clickProductAttributesTab() {
		productAttributesTab.click();
	}
	
	public boolean productAttributesTabVisible() {
		return tabProductAttributesInfo.isDisplayed();
	}
	
	public void expandAttributesList() {
		expandAttributesButton.click();
	}
	
	public void searchInAttributeList(String attribute) {
		attributeSearchBar.sendKeys(attribute);
	}
	
	public void waitForAttributeSearchBar(WebDriverWait wait) {
		this.wait = wait;
		
		try {
			wait.until(ExpectedConditions.visibilityOf(visibleAddAttributeIcon));
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex) {
			wait.until(ExpectedConditions.visibilityOf(visibleAddAttributeIcon));
		}		
	}
	
	public boolean checkAttributeIsFound(String attribute) {
		if(attributeToAdd.isDisplayed() && attributeToAdd.getText().contains(attribute)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void addFoundAttribute(String attribute) throws InterruptedException {
	
		try {
			visibleAddAttributeIcon.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex) {
			visibleAddAttributeIcon.click();
		}
	}
	
	public void waitForAttributeToBeAddded(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(addedAttributesTable));
	}
	
	public String getAddedAttributeName() {
		return addedAttributesTable.getText();
	}
	
	public void checkOptionAffectingCheckbox(WebDriverWait wait) throws InterruptedException {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(optionAffectingCheckbox));
		wait.until(ExpectedConditions.elementToBeClickable(optionAffectingCheckbox));
		Thread.sleep(2000);
		optionAffectingCheckbox.click();
		Thread.sleep(2000);
	}
	
	//--------------------------------------------------------------------------
	
	
	/*
		PPRODUCT PARAMETERS TAB ACTIONS
	*/	
	public void clickProductParametersTab() {
		productParametersTab.click();
	}
	
	public boolean productParametersTabVisible() {
		return tabProductParametersInfo.isDisplayed();
	}
	
	//--------------------------------------------------------------------------
	
	
	/*
		ATTRIBUTE DEFAULT VALUES TAB ACTIONS
	*/
	public void clickAttributeDefaultValuesTab() {
		attributeDefaultValuesTab.click();
	}
	
	public boolean attributeDefaultValueTabViible() {
		return tabAttributeDefaultValuesInfo.isDisplayed();
	}
	
	//--------------------------------------------------------------------------
	
	
	/*
		ATTRIBUTE VALUE DEPENDENCIES TAB ACTIONS
	 */
	public void clickAttributeValueDependenciesTab() {
		attributeValueDependenciesTab.click();
	}
	
	public boolean attributeValueDependenciesTabVisible() {
		return tabAttributeValueDependenciesInfo.isDisplayed();
	}
	
	//--------------------------------------------------------------------------
	
	
	/*
		PRODUCT OPTIONS TAB ACTIONS
	*/
	public void clickProductOptionsTab(WebDriverWait wait) throws InterruptedException {
		this.wait = wait;
		
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", savedOptionsTab);
		Thread.sleep(1000);
		
		wait.until(ExpectedConditions.visibilityOf(savedOptionsTab));
		wait.until(ExpectedConditions.elementToBeClickable(savedOptionsTab));
		
		savedOptionsTab.click();
	}
	
	public boolean productOptionsTabVisible() {
		return tabOptionsInfo.isDisplayed();
	}
	
	public void clickGenerateOptionsButton(WebDriverWait wait) throws InterruptedException {
		this.wait = wait;
		
		Thread.sleep(1000);
		
		wait.until(ExpectedConditions.elementToBeClickable(generateOptionsButton));
		
		try {
			generateOptionsButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex) {
			generateOptionsButton.click();
		}
		
		Thread.sleep(2000);
		
		if(!availableOptionsTable.isDisplayed()) {
			try {
				generateOptionsButton.click();
			}
			catch(org.openqa.selenium.StaleElementReferenceException ex) {
				generateOptionsButton.click();
			}
			Thread.sleep(2000);
		}
	}
	
	public void clickAddAllButton() throws InterruptedException {
		Thread.sleep(2000);
		addAllOptionsButton.click();
		Thread.sleep(2000);
	}
	
	public boolean checkCreatedOptions(WebDriverWait wait, String opt1, String opt2) throws InterruptedException {
		this.wait = wait;
		
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.visibilityOf(generatedOptionsTabel));
		
		if(generatedOptionsTabel.getText().contains(opt1) &&
				generatedOptionsTabel.getText().contains(opt2)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkSavedOptions(WebDriverWait wait, String opt1, String opt2) throws InterruptedException {
		this.wait = wait;
		
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.visibilityOf(savedOptionsTab));
		
		if(savedOptionsTable.getText().contains(opt1) &&
			savedOptionsTable.getText().contains(opt2)) {
			return true;
		}
		else {
			return false;
		}
	}
	
		
	
	//--------------------------------------------------------------------------
	
	
	/*
		IMAGES TAB ACTIONS
	*/
	public void clickImagesTab() {
		imagesTab.click();
	}
	
	public boolean imagesTabVisible() {
		return tabImagesInfo.isDisplayed();
	}
	
}
