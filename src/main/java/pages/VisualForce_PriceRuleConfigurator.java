package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class VisualForce_PriceRuleConfigurator {
	
	WebDriver driver;
	WebDriverWait wait;
	
	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
	
	/*
	 * 	BUTTONS
	 */
	@FindBy(id = "j_id0:j_id9:j_id11")
	WebElement saveAndReturnButton;
	
	@FindBy(id = "j_id0:j_id9:j_id13")
	WebElement saveButton;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[1]/div/div[2]/div/a[3]")
	WebElement cancelButton;
	
	//--------------------------------------------------------------------------

	/*
	 * 	FIELDS
	 */
	@FindBy(id = "j_id0:j_id9:j_id47")
	WebElement priceRuleNameField;
	
	//--------------------------------------------------------------------------	
	
	/*
	 * 	LOOKUPS
	 */
	@FindBy(id = "j_id0:j_id9:j_id57")
	WebElement productLookup;
	
	@FindBy(id = "j_id0:j_id9:j_id59")
	WebElement accountLookup;
	
	//##########################################################################
	//	RULES CONDITIONS SECTION ELEMENTS
	//##########################################################################
	
	/*
	 * 	BUTTONS
	 */
	@FindBy(id = "j_id0:j_id9:j_id115")
	WebElement addRuleConditionButton;

	//--------------------------------------------------------------------------	
	
	/*
	 * 	PICKLISTS
	 */
	@FindBy(name = "j_id0:j_id9:j_id72:0:j_id77")
	WebElement ruleCondition_AttributePicklist;
	
	@FindBy(name = "j_id0:j_id9:j_id72:0:j_id81")
	WebElement ruleCondition_FieldNamePicklis;
	
	@FindBy(name = "j_id0:j_id9:j_id72:0:j_id85")
	WebElement ruleCondition_OperatorPicklist;
	
	@FindBy(name = "j_id0:j_id9:j_id72:0:j_id99")
	WebElement ruleCondition_ValuePicklist;
	
	//##########################################################################
	//	PRICE RULE SECTION ELEMENTS
	//##########################################################################

	/*
	 * 	BUTTONS
	 */
	@FindBy(id = "j_id0:j_id9:j_id153")
	WebElement addRuleActionbutton;

	//--------------------------------------------------------------------------

	/*
	 * 	FIELDS
	 */
	@FindBy(name = "j_id0:j_id9:j_id123:0:j_id140")
	WebElement priceRuleActions_TargetValueField;

	//--------------------------------------------------------------------------	
	
	/*
	 * 	PICKLISTS
	 */
	@FindBy(name = "j_id0:j_id9:j_id123:0:j_id128")
	WebElement priceRuleActions_AttributePicklist;
	
	@FindBy(name = "j_id0:j_id9:j_id123:0:j_id132")
	WebElement priceRuleActions_FiledNamePicklist;
	
	@FindBy(name = "j_id0:j_id9:j_id123:0:j_id136")
	WebElement priceRuleActions_ActiontypePicklist;
	
	
	//##########################################################################	
	//	PAGE CONSTRUCTOR
	//##########################################################################	
	
	public VisualForce_PriceRuleConfigurator(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}

	//##########################################################################
	//	PAGE ACTIONS	
	//##########################################################################
	
	public void setPriceRuleName(String name) {
		priceRuleNameField.sendKeys(name);
	}
	
	public void setRuleProdut(String productName) {
		productLookup.sendKeys(productName);
	}
	
	public void setRuleAccount(String accountName) {
		accountLookup.sendKeys(accountName);
	}
	
	public void addPriceRuleCondition() throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", addRuleConditionButton);
		
		Thread.sleep(2000);
		addRuleConditionButton.click();
	}
	
	public void setPriceRuleConditionsAttribute(String attributeName, WebDriverWait wait) throws InterruptedException {
		Thread.sleep(2000);
		
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(ruleCondition_AttributePicklist));
		wait.until(ExpectedConditions.elementToBeClickable(ruleCondition_AttributePicklist));
		
		Select selectAttribute = new Select(ruleCondition_AttributePicklist);
		selectAttribute.selectByVisibleText(attributeName);
	}
	
	public void setPriceRuleConditionsOperator(WebDriverWait wait) throws InterruptedException {
		Thread.sleep(2000);
		
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(ruleCondition_OperatorPicklist));
		wait.until(ExpectedConditions.elementToBeClickable(ruleCondition_OperatorPicklist));
				
		Select selectOperator = new Select(ruleCondition_OperatorPicklist);
		selectOperator.selectByVisibleText("EQUALS");
	}
	
	public void setPriceRuleConditionsValue(String optionName, WebDriverWait wait) throws InterruptedException {
		Thread.sleep(2000);
		
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(ruleCondition_ValuePicklist));
		wait.until(ExpectedConditions.elementToBeClickable(ruleCondition_ValuePicklist));
		
		Select selectValue = new Select(ruleCondition_ValuePicklist);
		selectValue.selectByVisibleText(optionName);
	}
	
	public void addPriceRuleAction() throws InterruptedException {
		
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", addRuleActionbutton);
		Thread.sleep(1000);
		
		addRuleActionbutton.click();
	}
	
	public void setPriceRuleActionsAttribute(WebDriverWait wait, String attributeName) throws InterruptedException {
		this.wait = wait;
		
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.visibilityOf(priceRuleActions_AttributePicklist));
		wait.until(ExpectedConditions.elementToBeClickable(priceRuleActions_AttributePicklist));
		
		Select selectAttribute = new Select(priceRuleActions_AttributePicklist);
		selectAttribute.selectByVisibleText(attributeName);
	}
	
	public void setPriceRuleActionsFieldName() throws InterruptedException {
		Thread.sleep(2000);
		
		Select selectOperator = new Select(priceRuleActions_FiledNamePicklist);
		selectOperator.selectByVisibleText("Quantity");
	}
	
	public void setPriceRuleActionsActionType() throws InterruptedException {
		Thread.sleep(2000);
		
		Select selectOperator = new Select(priceRuleActions_ActiontypePicklist);
		selectOperator.selectByVisibleText("Set to value");
	}
	
	public void setPriceRuleActionsTargetValue(String value) throws InterruptedException {
		Thread.sleep(2000);
		
		priceRuleActions_TargetValueField.sendKeys(value);
	}
	
	public void clickSaveButton(WebDriverWait wait) throws InterruptedException {
		this.wait = wait;
		
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", saveButton);
		Thread.sleep(1000);
		
		wait.until(ExpectedConditions.visibilityOf(saveButton));
		wait.until(ExpectedConditions.elementToBeClickable(saveButton));
		saveButton.click();
	}
	
	public void clickSaveAndReturButton() throws InterruptedException {
		Thread.sleep(2000);
		
		saveAndReturnButton.click();
	}
	
	
	
	
	
}
