package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class React_QuoteConfigurator {
	
	WebDriver driver;
	WebDriverWait wait;

	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
	
	
	/*
	 * BUTTONS
	 */
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[1]/div/div[2]/div/a[1]/button")
	WebElement backToQuoteButton;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[1]/div/div[2]/div/a[2]/button")
	WebElement backToOpportunityButton;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div[2]/div/div[2]/div/table/tbody/tr/td[6]/a/span")
	WebElement addProductButton;
	
	//--------------------------------------------------------------------------
	
	
	/*
	 * PRODUCT CONFIGURATION ELEMENTS
	 */
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[1]/div[3]/div[1]")
	WebElement productNameInfo;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[1]/div[3]/div[3]/div/div/div/form/div/div/select")
	WebElement selectOptionPicklist;

	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[1]/div[3]/div[4]/span[2]/button[1]")
	WebElement calculatePricesButton;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[1]/div[3]/div[4]/span[2]/button[2]")
	WebElement addToBasketButton; 
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[1]/div[3]/div[4]/span[2]/button[1]")
	WebElement recalculateButton;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[1]/div[3]/div[4]/span[2]/button[2]")
	WebElement backButton; 

	//--------------------------------------------------------------------------
	
	/*
	 *	PRODUCT CART ELEMENTS
	 */
	
	@FindBy(xpath = "//*[@id=\"root\"]/div/routeroutlet/div/div/div[1]/div/a/button")
	WebElement checkoutButton;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div[2]/table/tbody/tr[1]/td[2]/a")
	WebElement cartProductName;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div[2]/table/tbody/tr[1]/td[3]")
	WebElement cartProductRecordtype;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div[2]/table/tbody/tr[1]/td[4]/span/input")
	WebElement cartProductQuantity;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div[2]/table/tbody/tr[1]/td[5]/div/input")
	WebElement cartProductSalesPrice;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div[2]/table/tbody/tr[1]/td[6]/div/input")
	WebElement cartProductTotalPrice;

	//--------------------------------------------------------------------------
	
	/*
	 * 	CHECKOUT PAGE ELEMENTS
	 */
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/form/div/div[2]/div/div[3]/div[1]/button")
	WebElement nextButton;
	
	@FindBy(name = "enxcpq__client_first_name__c")
	WebElement clientFirstNameField;
	
	@FindBy(name = "enxcpq__client_last_name__c")
	WebElement clientLastNameField;
	
	@FindBy(name = "enxcpq__client_email__c")
	WebElement clientEmailField;
	
	//Do zmiany po implementacji sortowania metod wysyłki i płatności
	@FindBy(xpath = "//*[@id=\"root\"]/div/routeroutlet/div/div/div/form/div/div[2]/div/div[1]/div[1]/span/label/span[1]")
	WebElement deliveryMethodCheckbox1;
	
	@FindBy(xpath = "//*[@id=\"root\"]/div/routeroutlet/div/div/div/form/div/div[2]/div/div[1]/div[1]/span")
	WebElement deliveryMethodCheckbox1Label;
	
	@FindBy(xpath = "//*[@id=\"root\"]/div/routeroutlet/div/div/div/form/div/div[2]/div/div[1]/div[2]/span/label/span[1]")
	WebElement deliveryMethodCheckbox2;
	
	@FindBy(xpath = "//*[@id=\"root\"]/div/routeroutlet/div/div/div/form/div/div[2]/div/div[1]/div[3]/span/label/span[1]")
	WebElement deliveryMethodCheckbox3;
	
	//--------------------------------------------------------------------------
	
	
	/*
	 * 	SUMMARY ELEMENTS
	 */
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[2]/div[1]/table/tbody/tr[1]/td[1]/ul/li[1]")
	WebElement summaryClientFirstNameField;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[2]/div[1]/table/tbody/tr[1]/td[1]/ul/li[2]")
	WebElement summaryClientLastNameField;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[2]/div[1]/table/tbody/tr[1]/td[1]/ul/li[3]")
	WebElement summaryClientEmailField;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[2]/div[1]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[1]")
	WebElement summaryProductName;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[2]/div[1]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[2]")
	WebElement summaryQuantity;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[2]/div[1]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[3]")
	WebElement summarySalesPrice;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div[1]/div[2]/div/routeroutlet/div/div/div/div/div[2]/div[1]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[4]")
	WebElement summaryTotalPrice;
	
	
	//##########################################################################
	//	PAGE CONSTRUCTOR
	//##########################################################################
		
		
	public React_QuoteConfigurator(WebDriver driver) {
		this.driver = driver;
			
		PageFactory.initElements(driver, this);
	}
		
		
	//##########################################################################
	// PAGE ACTIONS
	//##########################################################################
	
	public void clickAddProductButton(WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(addProductButton));
		wait.until(ExpectedConditions.elementToBeClickable(addProductButton));
		addProductButton.click();
	}
	
	public void clickCalculatePricesButton() throws InterruptedException {
		calculatePricesButton.click();
		Thread.sleep(2000);
	}
	
	public void clickAddToBasketButton() {
		addToBasketButton.click();
	}
	
	public void clickChcekoutButton(WebDriverWait wait) throws InterruptedException {
		Thread.sleep(1000);
		
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(checkoutButton));
		wait.until(ExpectedConditions.elementToBeClickable(checkoutButton));
		checkoutButton.click();
	}
	
	public void clickNextButton() {
		nextButton.click();
	}
	
	public void selectOption(String optionName) {
		Select option = new Select(selectOptionPicklist);
		option.selectByVisibleText(optionName);
	}
	
	public void selectDeliveryMethod() throws InterruptedException {
		
		Thread.sleep(1000);
		
		if (deliveryMethodCheckbox1Label.getText().contains("Odbiór osobisty")) {
			deliveryMethodCheckbox1.click();
		}
		else {
			deliveryMethodCheckbox3.click();
		}
		
	}
	
	public void setClientFirstName(String firstName) {
		clientFirstNameField.sendKeys(firstName);
	}
	
	public void setClientLastName(String lastName) {
		clientLastNameField.sendKeys(lastName);
	}
	
	public void setClientEmail(String email) {
		clientEmailField.sendKeys(email);
	}
	
	/*
	 *	CART CHECKERS 
	 */
	
	public boolean checkProductName(String productName, WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(productNameInfo));
		
		if(productNameInfo.getText().contains(productName)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkCartProductName(String productName, WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(cartProductName));
		
		if(cartProductName.getText().contains(productName)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkCartProductrecordType(String recordType, WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(cartProductRecordtype));
		
		if(cartProductRecordtype.getText().contains(recordType)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkCartQuantity(String quantity, WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(cartProductQuantity));
		
		if(cartProductQuantity.getAttribute("value").contains(quantity)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkCartSalesPrice(String listPrice, String modifier, WebDriverWait wait) {
		this.wait = wait;
		
		int numListPrice = Integer.parseInt(listPrice);
		int numModifier = Integer.parseInt(modifier);
		
		String salesPrice = Integer.toString(numListPrice + numModifier); 
		
		wait.until(ExpectedConditions.visibilityOf(cartProductSalesPrice));
		
		if(cartProductSalesPrice.getAttribute("value").contains(salesPrice)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkCartTotalPrice(String quantity, String listPrice, String modifier,  WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(cartProductTotalPrice));
		
		int numQuantity = Integer.parseInt(quantity);
		int numListPrice = Integer.parseInt(listPrice);
		int numModifier = Integer.parseInt(modifier);
		
		String totalPrice = Integer.toString((numQuantity * (numListPrice + numModifier)));
		
		if(cartProductTotalPrice.getAttribute("value").contains(totalPrice)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/*
	 *	SUMMARY CHECKERS
	 */

	public boolean checkSummaryClientFirstName(String firstName, WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(summaryClientFirstNameField));
		
		if(summaryClientFirstNameField.getText().contains(firstName)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkSummaryClientLastName(String lastName, WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(summaryClientLastNameField));
		
		if(summaryClientLastNameField.getText().contains(lastName)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkSummaryClientEmail(String email, WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(summaryClientEmailField));
		
		if(summaryClientEmailField.getText().contains(email)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkSummaryProductName(String productName, WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(summaryProductName));
		
		if(summaryProductName.getText().contains(productName)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkSummaryQuantity(String quantity, WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(summaryQuantity));
		
		if(summaryQuantity.getText().contains(quantity)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkSummarySalesPrice(String listPrice, String modifier, WebDriverWait wait) {
		this.wait = wait;
		
		int numListPrice = Integer.parseInt(listPrice);
		int numModifier = Integer.parseInt(modifier);
		
		String salesPrice = Integer.toString(numListPrice + numModifier); 
		
		wait.until(ExpectedConditions.visibilityOf(summarySalesPrice));
		
		if(summarySalesPrice.getText().contains(salesPrice)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkSummaryTotalPrice(String quantity, String listPrice, String modifier,  WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(summaryTotalPrice));
		
		int numQuantity = Integer.parseInt(quantity);
		int numListPrice = Integer.parseInt(listPrice);
		int numModifier = Integer.parseInt(modifier);
		
		String totalPrice = Integer.toString((numQuantity * (numListPrice + numModifier)));
		
		if(summaryTotalPrice.getText().contains(totalPrice)) {
			return true;
		}
		else {
			return false;
		}
	}
}
