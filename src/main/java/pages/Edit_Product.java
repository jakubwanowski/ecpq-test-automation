package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Edit_Product {

	//##########################################################################
	//	NESCESSARY OBJECTS
	//##########################################################################
	
	WebDriver driver;
	WebDriverWait wait;
	
	
	//##########################################################################	
	//	PAGE CONSTRUCTOR
	//##########################################################################	
	
	public Edit_Product(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	
	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
	
	/*
	 * 	BUTTONS
	 */
	@FindBy(name = "save")
	WebElement saveButton;
	
	@FindBy(name = "save_new")
	WebElement saveAndAddPriceButton;
	
	@FindBy(name = "cancel")
	WebElement cancelButton;
	

	/*
	 * 	FIELDS
	 */
	@FindBy(name = "Name")
	WebElement productNameField;
	
	@FindBy(name = "ProductCode")
	WebElement productCodeField;
	
	@FindBy(id = "00N9E000000TwAV")
	WebElement techExternalIdField;
	
	@FindBy(id = "00N9E000001CLAi")
	WebElement currentInventoryField;
	
	@FindBy(id = "Description")
	WebElement productDescriptionField;
	
	@FindBy(id = "00N9E000000TwAU")
	WebElement techDefinitionIdField;
	
	@FindBy(id = "00N9E000000TwAW")
	WebElement techOptionJsonField;
	
	
	/*
	 * 	PICKLISTS
	 */
	@FindBy(name = "CurrencyIsoCode")
	WebElement productCurrencyPicklist;
	
	@FindBy(id = "00N9E000001CLAj")
	WebElement currentLeadTimePicklist;
	
	
	/*
	 * 	LOOKUPS
	 */
	@FindBy(id = "CF00N9E000000TwAS")
	WebElement categoryLookupField;
	@FindBy(id = "CF00N9E000000TwAS_lkwgt")
	WebElement categoryLookupIcon;
	
	
	/*
	 * 	CHECKBOXS
	 */
	@FindBy(id = "IsActive")
	WebElement activeCheckbox;
	
	@FindBy(id = "00N9E000001CLAl")
	WebElement ignoreInventoryManagementCheckbox;
	
	@FindBy(id = "00N9E000000UUZB")
	WebElement ignoreOptionRequirementCheckbox;
	
	
	/*
	 * 	OTHERS
	 */
	@FindBy(xpath = "/html/body/div[1]/div[2]/table/tbody/tr/td[2]/form/div/div[2]/div[3]/table/tbody/tr[1]/td[4]")
	WebElement productRecordTypeInfo;
	

	//##########################################################################
	//	PAGE ACTIONS	
	//##########################################################################
	
	public void clickSaveButton() {
		saveButton.click();
	}
	
	public String getProductTypeInfo() {
		return productRecordTypeInfo.getText();
	}
	
	//	Set something
	//--------------------------------------------------------------------------	
	public void setProductName(String name) {
		productNameField.sendKeys(name);
	}
	
	public void setProductCurrency(String currency) {
		Select productCurrency = new Select(productCurrencyPicklist);
		productCurrency.selectByValue(currency);
	}
	
	public void setProductAsActive() {
		activeCheckbox.click();
	}
	
	public void waitForPageToLoad(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(productRecordTypeInfo));
	}

}
