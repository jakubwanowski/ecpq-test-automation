package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Edit_Account {

	//##########################################################################
	//	NESCESSARY OBJECTS
	//##########################################################################		
	
	WebDriver driver;
	WebDriverWait wait;


	//##########################################################################
	//	PAGE CONSTRUCTOR
	//##########################################################################
		
	public Edit_Account(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}

	
	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
	
	/*
	 * 	BUTTONS
	 */
	@FindBy(name = "save")
	WebElement saveButton;
	
	@FindBy(name = "save_new")
	WebElement saveAndNewButton;
	
	@FindBy(name = "cancel")
	WebElement cancelButton;
	
	
	/*
	 * 	FIELDS
	 */
	@FindBy(id = "acc2")
	WebElement accountNameField;
	
	
	/*
	 * 	PICKLISTS	
	 */
	@FindBy(id = "acc24")
	WebElement accountCurrencyPicklist;
	
		
	//##########################################################################
	//	PAGE ACTIONS
	//##########################################################################
	
	public void clickSaveButton() {
		saveButton.click();
	}
	
	public void setAccountName(String name) {
		accountNameField.sendKeys(name);
	}
	
	public void waitForPageToLoad(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(accountNameField));
	}
	
}
