package pages;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeveloperConsole {

	//##########################################################################
	//	NESCESSARY OBJECTS
	//##########################################################################	
	
	WebDriver driver;
	WebDriverWait wait;
	
	
	//##########################################################################
	//	PAGE CONSTRUCTOR
	//##########################################################################
		
	public DeveloperConsole(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	
	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
	
	@FindBy(id = "debugMenuEntry")
	WebElement debugTab;
	
	
	/*
	 * 	DEBUG TAB
	 */
	@FindBy(id = "openExecuteAnonymousWindow")
	WebElement openExecuteAnonymousWindowOption;
	
	@FindBy(id = "executeLast")
	WebElement executeLastOption;
	
	
	/*
	 * 	ENTER APEX CODE WINDOW
	 */
	@FindBy(xpath = "/html/body/div[9]/div[2]/div/div[1]/div/div/div")
	WebElement scriptTextArea;
	
	@FindBy(id = "button-1188")
	WebElement executeButton;
	

	//##########################################################################
	//	PAGE ACTIONS
	//##########################################################################
	
	//	Click something
	//--------------------------------------------------------------------------
	public void clickExecuteAnonymousWindowOption() {
		openExecuteAnonymousWindowOption.click();
	}

	public void clickExecuteLatsOption() {
		executeLastOption.click();
	}
	
	public void clickExecuteButton() {
		executeButton.click();
	}
	
	public String clearTestDataScript() {
		return "\r\n" +
				"List<Account> accToDelete = \r\n" + 
				"    [SELECT Id, Name FROM Account WHERE Name = 'autoAcc'];\r\n" + 
				"List<enxCPQ__Attribute__c> atrToDelete = [SELECT Id, Name FROM enxCPQ__Attribute__c WHERE Name = 'autoAtr'];\r\n" + 
				"List<Product2> prdToDelete = \r\n" + 
				"    [SELECT Id, Name FROM Product2 WHERE Name = 'autoPrd']; \r\n" + 
				"List<Pricebook2> pricebookToDelete = \r\n" + 
				"    [SELECT Id, Name FROM Pricebook2 WHERE Name = 'autoMasterPricebook' OR Name = 'autoPartnerPricebook'];\r\n" + 
				"List<enxCPQ__PriceRule__c> priceRulesToDelete = \r\n" + 
				"    [SELECT Id, Name FROM enxCPQ__PriceRule__c WHERE Name = 'autoPriceRule']; \r\n" + 
				"List<Opportunity> oppToDelete = \r\n" + 
				"    [SELECT Id, Name FROM Opportunity WHERE Name = 'autoOpp'];\r\n" + 
				"List<enxCPQ__QuoteLineItemAttribute__c> qliToDelete = \r\n" + 
				"    [SELECT Id, Name FROM enxCPQ__QuoteLineItemAttribute__c WHERE Name = 'autoAtr'];\r\n" + 
				"\r\n" + 
				"delete qliToDelete;\r\n" + 
				"delete oppToDelete;\r\n" + 
				"delete pricebookToDelete;\r\n" + 
				"delete priceRulesToDelete;\r\n" + 
				"delete accToDelete;\r\n" + 
				"delete atrToDelete;\r\n" + 
				"delete prdToDelete;";
	}
	
	public void madeScriptTextAreaEditable() {
		((JavascriptExecutor)driver).executeScript("document.getElementById(\"panel-1183\").contentEditable = true;");
	}
	
	public void extandDebugTab(WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.elementToBeClickable(debugTab));
		debugTab.click();
	}
	
	public void executeScript(String scriptBody) throws InterruptedException {
		Thread.sleep(2000);
		
		//scriptTextArea.clear();
		//goToScriptTextArea();
		scriptTextArea.sendKeys(scriptBody);
	}
	
	public void goToScriptTextArea() throws InterruptedException {
		Thread.sleep(2000);
		
		scriptTextArea.click();
	}
	
	public void switchWindows(WebDriverWait wait) throws InterruptedException {
		this.wait = wait;
		
		String subWindowHandler = null;
		
		Set<String> handles = driver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		
		while(iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}
		
		driver.switchTo().window(subWindowHandler);
	}
	
}
