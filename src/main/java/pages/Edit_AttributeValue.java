package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Edit_AttributeValue {

	//##########################################################################
	//	NESCESSARY OBJECTS
	//##########################################################################
	
	WebDriver driver;
	WebDriverWait wait;

	
	//##########################################################################
	//	PAGE CONSTRUCTOR
	//##########################################################################
		
	public Edit_AttributeValue(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	
	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
		
	/*
	 *	BUTTONS
	 */
	@FindBy(name = "save")
	WebElement saveButton;
	
	@FindBy(name = "save_new")
	WebElement saveAndNewButton;
	
	@FindBy(name = "cancel")
	WebElement cancelButton;
	
	/*
	 *	FIELDS
	 */
	@FindBy(id = "Name")
	WebElement attributeValueNameField;
	
	
	/*
	 *	PICKLISTS
	 */	
	@FindBy(id = "CurrencyIsoCode")
	WebElement currencyPicklist;
	
	
	/*
	 * 	LOOKUPS
	 */
	@FindBy(id = "CF00N9E000000Tw8E")
	WebElement attributeLookupField;
	
	@FindBy(id = "CF00N9E000000Tw8E_lkwgt")
	WebElement attributeLookupIcon;
	

	//##########################################################################
	//	PAGE ACTIONS	
	//##########################################################################
	
	public void clickSaveButton() {
		saveButton.click();
	}
	
	public void clicksaveAndNewButton() {
		saveAndNewButton.click();
	}
	
	public void setAttributeValueName(String name) {
		attributeValueNameField.sendKeys(name);
	}
	
	public void waitForPageToLoad(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(attributeValueNameField));
	}
	
}
