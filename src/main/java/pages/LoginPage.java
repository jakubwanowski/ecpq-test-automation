package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	WebDriver driver;

	//##########################################################################
	//	Page Elements
	//##########################################################################
	
	/*
		Buttons
	*/
	@FindBy(id = "Login")
	WebElement loginButton;
	
	//--------------------------------------------------------------------------
	
	/*
		Fields
	*/
	@FindBy(id = "username")
	WebElement userNameField;
	
	@FindBy(id = "password")
	WebElement passwordField;

	
	
	//##########################################################################
	//	Page constructor
	//##########################################################################
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	//##########################################################################
	//	Page actions
	//##########################################################################
	
	//Set user name
	public void setUserName(String userName) {
		userNameField.clear();
		userNameField.sendKeys(userName);
	}
	
	//Set password
	public void setPassword(String password) {
		passwordField.clear();
		passwordField.sendKeys(password);
	}
	
	//Click login
	public void clickLogin() {
		loginButton.click();
	}

}
