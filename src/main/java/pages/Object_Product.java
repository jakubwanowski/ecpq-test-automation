package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Object_Product {
	
	WebDriver driver;
	WebDriverWait wait;

	//##########################################################################
	//	Page Elements
	//##########################################################################
	
	/*
		Buttons
	*/
	@FindBy(name = "edit")
	WebElement editButton;
	
	@FindBy(name = "del")
	WebElement deleteButton;
	
	@FindBy(name = "clone")
	WebElement cloneButton;
	
	@FindBy(name = "enxcpq__product_design")
	WebElement productDesignButton;
	
	//--------------------------------------------------------------------------
	
	/*
		Fields
	*/
	
	//--------------------------------------------------------------------------
	
	/*
		Other
	*/
	@FindBy(id = "Name_ileinner")
	WebElement productNameInfo;
	
	//##########################################################################
	//	Page constructor
	//##########################################################################
	
	public Object_Product(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	//##########################################################################
	// Page actions
	//##########################################################################

	public void clickProductDesign() {
		productDesignButton.click();
	}
	
	public String getProductNameInfo() {
		return productNameInfo.getText();
	}
	
	public void waitForPageToLoad(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(productNameInfo));
	}
	
	
}
