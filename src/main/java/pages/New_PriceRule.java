package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class New_PriceRule {
	WebDriver driver;
	WebDriverWait wait;
	
	//##########################################################################
	//	Page Elements
	//##########################################################################
	
	/*
	 * 	Buttons
	 */
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td[2]/form/div/div[3]/table/tbody/tr/td[2]/input[1]")
	WebElement continueButton;
	
	@FindBy(name = "cancel")
	WebElement cancelButton;
	
	//--------------------------------------------------------------------------
	
	/*
	 * 	Picklists
	 */
	@FindBy(id = "p3")
	WebElement productRecordTypePicklist;
	
	
	//##########################################################################	
	//	Page constructor
	//##########################################################################	
	
	public New_PriceRule(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}

	//##########################################################################
	// Page actions
	//##########################################################################
	
	public void clickContinueButton() {
		continueButton.click();
	}
	
	public void clickCancelButton() {
		cancelButton.click();
	}
	
}
