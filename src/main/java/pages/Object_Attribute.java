package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Object_Attribute {
	
	WebDriver driver;
	WebDriverWait wait;

	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################
	
	/*
		BUTTONS
	*/
	@FindBy(name = "edit")
	WebElement editButton;
	
	@FindBy(name = "del")
	WebElement deleteButton;
	
	@FindBy(name = "clone")
	WebElement cloneButton;
	
	//--------------------------------------------------------------------------
	
	/*
		RELATED LISTS ELEMENTS
	 */
	
	//	ATTRIBUTE VALUES RELATED LIST
	@FindBy(name = "new00N9E000000Tw8E")
	WebElement newAttributeValueButton;
	
	@FindBy(css = ".listRelatedObject.Custom8Block")
	WebElement relatedListBlock;
	
	//--------------------------------------------------------------------------
	
	/*
		OTHERS
	 */
	@FindBy(id = "Name_ileinner")
	WebElement attributeNameInfo;
	
	@FindBy(id = "00N9E000000Tw9Q_ileinner")
	WebElement attributeTypeInfo;
	
	//##########################################################################
	//	PAGE CONSTRUCTOR
	//##########################################################################
		
	public Object_Attribute(WebDriver driver) {
		this.driver = driver;
			
		PageFactory.initElements(driver, this);
	}
	
	//##########################################################################
	// PAGE ACTIONS
	//##########################################################################

	public String getAttributeNameInfo() {
		return attributeNameInfo.getText();
	}
	
	public String getAttributeTypeInfo() {
		return attributeTypeInfo.getText();
	}
	
	public void clickNewAttributeValueButton() throws InterruptedException {
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", relatedListBlock);
			Thread.sleep(2000);
			newAttributeValueButton.click();
	}
	
}
