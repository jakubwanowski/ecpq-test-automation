package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class React_PricebookManagementTool {

	WebDriver driver;
	WebDriverWait wait;
	
	//##########################################################################
	//	PAGE ELEMENTS
	//##########################################################################

	
	/*
	 *	BUTTONS
	 */
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[5]/div/div/div[1]/div/div/a")
	WebElement advancedOptionsButton;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[5]/div/div/div[2]/div/div/button[1]")
	WebElement createNewPricebookButton;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[5]/div/div/div[2]/div/div/button[3]")
	WebElement saveButton;
	
	//--------------------------------------------------------------------------
	
	
	/*
	 *	FIELDS
	 */
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[4]/div/div[1]/div/div/div/table/tbody/tr[1]/td[6]")
	WebElement productListPriceField;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[4]/div/div[1]/div/div/div/table/tbody/tr[1]/td[6]")
	WebElement productPriceModifierField;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[4]/div/div[1]/div/div/div/table/tbody/tr[2]/td[6]")
	WebElement option1ListPriceField;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[4]/div/div[1]/div/div/div/table/tbody/tr[2]/td[7]")
	WebElement option1PriceModifierField;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[4]/div/div[1]/div/div/div/table/tbody/tr[3]/td[6]")
	WebElement option2ListPriceField;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[4]/div/div[1]/div/div/div/table/tbody/tr[3]/td[8]")
	WebElement option2PriceOverideField;

	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[4]/div/div[1]/div/div/div/table/tbody/tr[3]/td[7]")
	WebElement a1Field;
	//--------------------------------------------------------------------------
	
	
	/*
	 * 	PICKLISTS
	 */
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[3]/div/div/div/div[1]/div[4]/div/div/div[1]/div[1]/div/div[1]/div/div/button")
	WebElement currenciesPicklistButton;
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[3]/div/div/div/div[1]/div[4]/div/div/div[1]/div[1]/div/div[2]/div/div/ul/li[1]")
	WebElement euroCurrency;
	
	//--------------------------------------------------------------------------
	
	
	/*
	 *	LOOKUPS
	 */
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[3]/div/div/div/div[1]/div[1]/div/div/div/div[1]/div/div/div/input")
	WebElement selectPricebookLookup;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[3]/div/div/div/div[2]/div[3]/div/div/div/div/input")
	WebElement searchProductByName;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[3]/div/div/div/div[1]/div[3]/div/div/div/div/div[1]/div/div/div/input")
	WebElement searchReferenceMasterPricebook;
	
	//--------------------------------------------------------------------------
	
	
	/*
	 *	CHECKBOXS
	 */
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[3]/div/div/div/div[1]/div[5]/div/div/div/div/label/span[2]/span[1]")
	WebElement showAllProductsCheckbox;
	
	
	/*
	 * 	OTHERS
	 */
	@FindBy(css = ".slds-page-header__title.slds-truncate")
	WebElement toolNameInfo;
	
	@FindBy(xpath = "//*[@id=\"pricebookToolbar\"]/div/div/div/div[1]/div[2]/div/div/b[1]/span[1]")
	WebElement selectedPricebookName;
	
	//--------------------------------------------------------------------------
	
	/*
	 * 	CREATE NEW PRICEBOOK FRAME
	 */
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div/div[1]/h2")
	WebElement frameInfo;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div/div[3]/button[2]")
	WebElement saveCreatedPricebookButton;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div/div[3]/button[1]")
	WebElement cancelButton;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div/div[2]/div/div/div[3]/div/div/div/input")
	WebElement pricebookNameField;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div/div[2]/div/div/div[4]/div/div/div/textarea")
	WebElement descriptionField;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div/div[2]/div/div/div[5]/div[1]/div/div/div[1]/div/div/input")
	WebElement validFromField;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div/div[2]/div/div/div[5]/div[2]/div/div/div[1]/div/div/input")
	WebElement validToField;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div/div[2]/div/div/div[1]/div[2]/div/div[1]/div/div/label/span[2]/span[1]")
	WebElement masterCheckbox;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div/div[2]/div/div/div[1]/div[2]/div/div[2]/div/div/label/span[2]/span[1]")
	WebElement useUnitPriceCheckbox;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div/div/button")
	WebElement referencePricebookManagementFields;
	
	@FindBy(xpath = "/html/body/div/div[2]/table/tbody/tr/td/form/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div/ul/li[3]")
	WebElement referencePricebookManagement_ListPrice;
	
	
	//##########################################################################
	//	PAGE CONSTRUCTOR
	//##########################################################################
	
	
	public React_PricebookManagementTool(WebDriver driver) {
		this.driver = driver;
		
		PageFactory.initElements(driver, this);
	}
	
	
	//##########################################################################
	// PAGE ACTIONS
	//##########################################################################
	
	
	public String getToolNameInfo() {
		return toolNameInfo.getText();
	}
	
	public String getSelectedPricebookName() {
		return selectedPricebookName.getText();
	}
	
	public void clickCreateNewPricebook(WebDriverWait wait) {
		this.wait = wait;
		
		wait.until(ExpectedConditions.visibilityOf(createNewPricebookButton));
		wait.until(ExpectedConditions.elementToBeClickable(createNewPricebookButton));
		createNewPricebookButton.click();
	}
	
	public void waitForPageToLoad(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions.visibilityOf(toolNameInfo));
	}
	
	public void clickShowAllProductsCheckbox(WebDriverWait wait) throws InterruptedException {
		this.wait = wait;
		
		Thread.sleep(2000);
		
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", showAllProductsCheckbox);
		wait.until(ExpectedConditions.visibilityOf(showAllProductsCheckbox));
		wait.until(ExpectedConditions.elementToBeClickable(showAllProductsCheckbox));
		showAllProductsCheckbox.click();
	}
	
	public void expandCurrencyList(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions.elementToBeClickable(currenciesPicklistButton));
		currenciesPicklistButton.click();
	}
	
	public void setEuroCurrency(WebDriverWait wait) {
		this.wait = wait;
		try { 
			wait.until(ExpectedConditions.visibilityOf(euroCurrency));
			euroCurrency.click();
		}
		catch(Exception e) {
			expandCurrencyList(wait);
			wait.until(ExpectedConditions.visibilityOf(euroCurrency));
			euroCurrency.click();
		}	
	}
	
	public void searchProductByName(String name) throws InterruptedException {
		searchProductByName.clear();
		searchProductByName.sendKeys(name);
		Thread.sleep(3000);
	}
	
	public void selectReferenceMasterPricebook(String name) {
		
	}
	
	
	public void clickSaveButton() {
		saveButton.click();
	}
	
	public void setProductListPrice(String price) {
		Actions action = new Actions(driver);
		action.moveToElement(productListPriceField);
		action.click();
		action.sendKeys(price);
		action.build().perform();
		
		a1Field.click();
	}
	
	public void setOption1ListPrice(String price) {
		Actions action = new Actions(driver);
		action.moveToElement(option1ListPriceField);
		action.click();
		action.sendKeys(price);
		action.build().perform();
		
		a1Field.click();
	}

	public void setOption2ListPrice(String price) {
		Actions action = new Actions(driver);
		action.moveToElement(option2ListPriceField);
		action.click();
		action.sendKeys(price);
		action.build().perform();
		
		a1Field.click();
	}
	
	public void setProductPriceModifier(String price) {
		Actions action = new Actions(driver);
		action.moveToElement(productPriceModifierField);
		action.click();
		action.sendKeys(price);
		action.build().perform();
		
		a1Field.click();
	}
	
	public void setOption1PriceModifier(String price) {
		Actions action = new Actions(driver);
		action.moveToElement(option1PriceModifierField);
		action.click();
		action.sendKeys(price);
		action.build().perform();
		
		a1Field.click();
	}

	public void setOption2PriceOveride(String price) {
		Actions action = new Actions(driver);
		action.moveToElement(option2PriceOverideField);
		action.click();
		action.sendKeys(price);
		action.build().perform();
		
		a1Field.click();
	}

	
	//##########################################################################
	// CREATE NEW PRICEBOOK FRAME ACTIONS
	//##########################################################################	
	
	public String getFrameInfo() {
		return frameInfo.getText();
	}
	
	public void waitForFrameToLoad(WebDriverWait wait) {
		this.wait = wait;
		wait.until(ExpectedConditions
				.visibilityOf(frameInfo));
	}
	
	public void clickSaveCreatedPricebookButton(WebDriverWait wait) {
		this.wait = wait;
		
		descriptionField.click();
		
		wait.until(ExpectedConditions.elementToBeClickable(saveCreatedPricebookButton));
		saveCreatedPricebookButton.click();
	}
	
	public void setPricebookName(String name) {
		Actions action = new Actions(driver);
		action.moveToElement(pricebookNameField);
		action.click();
		action.build().perform();
		
		pricebookNameField.clear();
		pricebookNameField.sendKeys(name);
		
	}
	
	public void setValidFromField(String validFrom) {
		validFromField.clear();
		validFromField.sendKeys(validFrom);
	}
	
	public void setValidToField(String validTo) {
		validToField.clear();
		validToField.sendKeys(validTo);
	}
	
	public void setUseUnitPrice() {
		useUnitPriceCheckbox.click();
	}
	
	public void setPartnerPricebook() {
		masterCheckbox.click();
	}
	
	public void selectReferenceMasterPricebookFields_ListPrice() throws InterruptedException {
		referencePricebookManagementFields.click();
		
		Thread.sleep(500);
		
		referencePricebookManagement_ListPrice.click();
	}
	
	
}	
