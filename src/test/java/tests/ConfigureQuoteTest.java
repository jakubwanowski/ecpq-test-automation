package tests;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.React_QuoteConfigurator;
import settings.Config;

public class ConfigureQuoteTest extends Config {
	
	@Test(priority = 0)
	public void selectProductFromCatalogue() {
		quoteConfigurator.clickAddProductButton(wait);
	}
	
	@Test(priority = 10)
	public void configureProduct() throws InterruptedException {
		Assert.assertTrue(quoteConfigurator.checkProductName(productName, wait));
		quoteConfigurator.selectOption(attributeVal1);
		quoteConfigurator.clickCalculatePricesButton();
		
		Thread.sleep(3000);
		
		Assert.assertTrue(quoteConfigurator.checkProductName(attributeVal1, wait));
		quoteConfigurator.clickAddToBasketButton();
	}
	
	@Test(priority = 20)
	public void checkConfiguration() {
		Assert.assertTrue(quoteConfigurator.checkCartProductName(productName, wait));
		Assert.assertTrue(quoteConfigurator.checkCartProductrecordType(productRecordType, wait));
		Assert.assertTrue(quoteConfigurator.checkCartQuantity(adjustTargetValue, wait));
		Assert.assertTrue(quoteConfigurator.checkCartSalesPrice(option1ListPrice, option1PriceModifier, wait));
		Assert.assertTrue(quoteConfigurator.checkCartTotalPrice(adjustTargetValue, option1ListPrice, option1PriceModifier,wait));
	}
	
	@Test(priority = 30)
	public void checkoutProcess() throws InterruptedException {
		quoteConfigurator.clickChcekoutButton(wait);
		quoteConfigurator.setClientFirstName(clientFirstName);
		quoteConfigurator.setClientLastName(clientLastName);
		quoteConfigurator.setClientEmail(clientEmail);
		quoteConfigurator.selectDeliveryMethod();
		quoteConfigurator.clickNextButton();
	}
	
	@Test(priority = 40)
	public void checkOrderSummary() {
		Assert.assertTrue(quoteConfigurator.checkSummaryClientFirstName(clientFirstName, wait));
		Assert.assertTrue(quoteConfigurator.checkSummaryClientLastName(clientLastName, wait));
		Assert.assertTrue(quoteConfigurator.checkSummaryClientEmail(clientEmail, wait));
		Assert.assertTrue(quoteConfigurator.checkSummaryProductName(productName, wait));
		Assert.assertTrue(quoteConfigurator.checkSummaryQuantity(adjustTargetValue, wait));
		Assert.assertTrue(quoteConfigurator.checkSummarySalesPrice(option1ListPrice, option1PriceModifier, wait));
		Assert.assertTrue(quoteConfigurator.checkSummaryTotalPrice(adjustTargetValue, option1ListPrice, option1PriceModifier ,wait));
	}
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		
		wait = new WebDriverWait(driver, 10);
		quoteConfigurator = new React_QuoteConfigurator(driver);
	
	}
	

}
