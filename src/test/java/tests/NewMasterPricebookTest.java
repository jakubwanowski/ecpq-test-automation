package tests;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import junit.framework.Assert;
import pages.HomePage;
import pages.React_PricebookManagementTool;
import settings.Config;

public class NewMasterPricebookTest extends Config {
	
	@Test(priority = 0)
	public void getOnProcebookManagementToolPage() throws InterruptedException {
		home.clickPricebookManagementToolButton(wait);
		
		pricebookTool.waitForPageToLoad(wait);
		Assert.assertTrue(pricebookTool.getToolNameInfo()
				.contains("Pricebook Management Tool"));
	}
	
	@Test(priority = 10)
	public void openPricebookCreationFrame() throws InterruptedException {
		Thread.sleep(20000);
		pricebookTool.clickCreateNewPricebook(wait60);
		pricebookTool.waitForFrameToLoad(wait);
		Assert.assertTrue(pricebookTool.getFrameInfo()
				.contains("Create new Pricebook"));
	}
	
	@Test(priority = 20)
	public void createMasterPricebook() throws InterruptedException {
		pricebookTool.setPricebookName(masterPricebookName);
		pricebookTool.setUseUnitPrice();
		pricebookTool.setValidFromField(masterValidFromDate);
		pricebookTool.setValidToField(masterValidToDate);
		pricebookTool.clickSaveCreatedPricebookButton(wait);
		
		Thread.sleep(5000);
		//Assert.assertTrue(pricebookTool.getSelectedPricebookName().contains(masterPricebookName));
	}
	
	@Test(priority = 30)
	public void addProductsToMasterPricebook() throws InterruptedException {
		pricebookTool.clickShowAllProductsCheckbox(wait);
		pricebookTool.expandCurrencyList(wait);
		pricebookTool.setEuroCurrency(wait);
		pricebookTool.searchProductByName(productName);
		
		Thread.sleep(3000);
		pricebookTool.setProductListPrice(productListPrice);
		
		Thread.sleep(500);
		pricebookTool.setOption1ListPrice(option1ListPrice);
		
		Thread.sleep(500);
		pricebookTool.setOption2ListPrice(option2tListPrice);
		
		Thread.sleep(500);
		pricebookTool.clickSaveButton();
	}
	
	
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		wait = new WebDriverWait(driver, 20);
		wait60 = new WebDriverWait(driver, 60);
		home = new HomePage(driver);
		pricebookTool = new React_PricebookManagementTool(driver);
	}
	
	@AfterClass
	public void wait3Seconds() throws InterruptedException {
		Thread.sleep(1000);
	}
	
}

