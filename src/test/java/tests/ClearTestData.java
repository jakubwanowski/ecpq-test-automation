package tests;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.DeveloperConsole;
import pages.HomePage;
import settings.Config;

public class ClearTestData extends Config {
	
	@Test(priority = 0)
	public void openConsole() throws InterruptedException {
		home.expandUserNavigationMenu();
		home.openDeveloperConsole(wait);
		Thread.sleep(15000);
	}
	
//	
//	@Test(priority = 10)
//	public void openExecuteAnonymousWindow() throws InterruptedException {
//		console.switchWindows(wait);
//		console.extandDebugTab(wait);
//		console.clickExecuteAnonymousWindowOption();
//	}
//	
//	@Test(priority = 12)
//	public void executeApexScript() throws InterruptedException {		
//		console.goToScriptTextArea();
//		console.madeScriptTextAreaEditable();
//		console.executeScript(console.clearTestDataScript());
//		console.clickExecuteButton();
//	}
//	
	
	@Test(priority = 10)
	public void executeLastScript() throws InterruptedException {
		console.switchWindows(wait);
		console.extandDebugTab(wait);
		console.clickExecuteLatsOption();
	}
	
	
	@Test(priority = 20)
	public void closeDriver() throws InterruptedException {
		driver.close();
		console.switchWindows(wait);
	}
	
	
	
	@BeforeClass
	public void setup() {
		wait = new WebDriverWait(driver, 15);
		
		home = new HomePage(driver);
		console = new DeveloperConsole(driver);
	}

}
