package tests;

import settings.Config;
import pages.Edit_Product;
import pages.HomePage;
import pages.New_Product;
import pages.React_ProductDesignPage;
import pages.Object_Product;
import pages.Home_Products;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class NewProductTest extends Config{
	
	@Test(priority = 0)
	public void newProductPreparation() throws InterruptedException {
		
		home.clickProductsButton(wait);
		homePrd.clickNewButton();
		
		newProduct.waitForPageToLoad(wait);
		newProduct.selectRecordType(productRecordType);
		newProduct.clickContinueButton();
		
		editProduct.waitForPageToLoad(wait);

		Assert.assertEquals(editProduct.getProductTypeInfo(), productRecordType);
	}
	
	@Test(priority = 10)
	public void newProductDefinition() throws InterruptedException {
		
		editProduct.setProductName(productName);
		editProduct.setProductAsActive();
		editProduct.clickSaveButton();
		
		product.waitForPageToLoad(wait);
		  
		Assert.assertEquals(product.getProductNameInfo(), productName);
	}
	
	@Test(priority = 20)
	public void getOnProductDesignPage() throws InterruptedException {
		
		product.clickProductDesign();
		productDesign.waitForPageToLoad(wait);
		
		Assert.assertTrue(productDesign.getDesinedProductNameInfo().contains(productName));
	}
	
	@Test(priority = 30)
	public void setProductAttribute() throws InterruptedException {
		
		productDesign.expandAttributesList();
		productDesign.searchInAttributeList(attributeName);
		
		Thread.sleep(2000);
		
		productDesign.waitForAttributeSearchBar(wait);
		
		Thread.sleep(2000);
		
		productDesign.addFoundAttribute(attributeName);
		productDesign.waitForAttributeToBeAddded(wait);
		
		Thread.sleep(2000);
		
		productDesign.checkOptionAffectingCheckbox(wait);
		Assert.assertTrue(productDesign.getAddedAttributeName().contains(attributeName));
		
		Thread.sleep(2000);
		productDesign.clickQuickSave(wait);
	}
	
	@Test(priority = 40)
	public void createProductOptions() throws InterruptedException {
		
		productDesign.clickProductOptionsTab(wait);
		
		Thread.sleep(2000);
		
		productDesign.clickGenerateOptionsButton(wait);
		
		Thread.sleep(5000);
		
		//	Wymaga dopracowania
		//Assert.assertTrue(productDesign.checkCreatedOptions(wait, attributeVal1, attributeVal2));
		
		productDesign.clickAddAllButton();
		Thread.sleep(500);
		Assert.assertTrue(productDesign.checkSavedOptions(wait, attributeVal1, attributeVal2));
	}
	

	@BeforeClass(alwaysRun = true)
	public void setup() {
		wait = new WebDriverWait(driver, 10);
		wait60 = new WebDriverWait(driver, 60);
		
		home = new HomePage(driver);
		homePrd = new Home_Products(driver);
		newProduct = new New_Product(driver);
		editProduct = new Edit_Product(driver);
		product = new Object_Product(driver);
		productDesign = new React_ProductDesignPage(driver);
	}

}
