package tests;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import junit.framework.Assert;
import pages.React_PricebookManagementTool;
import settings.Config;

public class NewPartnerPricebookTest_Extended extends Config {
	
	@Test(priority = 10)
	public void openPricebookCreationFrame() throws InterruptedException {
		Thread.sleep(10000);
		pricebookTool.clickCreateNewPricebook(wait);
		pricebookTool.waitForFrameToLoad(wait);
		Assert.assertTrue(pricebookTool.getFrameInfo()
				.contains("Create new Pricebook"));
	}
	
	@Test(priority = 20)
	public void createPartnerPricebook() throws InterruptedException {
		Thread.sleep(1000);
		pricebookTool.setPricebookName(partnerPricebookName);
		
		Thread.sleep(1000);
		pricebookTool.setPartnerPricebook();
		
		/* EXTENDED PART */
		Thread.sleep(500);
		pricebookTool.selectReferenceMasterPricebookFields_ListPrice();
		
		Thread.sleep(2000);
		pricebookTool.clickSaveCreatedPricebookButton(wait);
		
		Thread.sleep(5000);
//		Assert.assertTrue(pricebookTool.getSelectedPricebookName()
//				.contains(partnerPricebookName));
	}
	
	@Test(priority = 30)
	public void addProductsToPartnerPricebook() throws InterruptedException {
		Thread.sleep(1000);
		pricebookTool.clickShowAllProductsCheckbox(wait);
		
		Thread.sleep(1000);
		pricebookTool.expandCurrencyList(wait);
		
		Thread.sleep(1000);
		pricebookTool.setEuroCurrency(wait);
		
		Thread.sleep(1000);
		pricebookTool.searchProductByName(productName);
		
		Thread.sleep(2000);
		pricebookTool.setProductPriceModifier(productPriceModifier);
		
		Thread.sleep(1000);
		pricebookTool.setOption1PriceModifier(option1PriceModifier);
		
		Thread.sleep(1000);
		pricebookTool.setOption2PriceOveride(option2tPriceOveride);
		
		Thread.sleep(1000);
		pricebookTool.clickSaveButton();
	}
	
	
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		wait = new WebDriverWait(driver, 20);
		pricebookTool = new React_PricebookManagementTool(driver);
	}	
}
