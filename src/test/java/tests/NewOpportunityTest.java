package tests;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.ChoosePricebook;
import pages.Edit_Opportunity;
import pages.HomePage;
import pages.Home_Opportunities;
import pages.Object_Opportunity;
import settings.Config;

public class NewOpportunityTest extends Config{

	@Test(priority = 0)
	public void newOpportunityPreparation() throws InterruptedException {
		Thread.sleep(4000);
		
		home.clickOpportunitiesButton(wait);
		homeOpp.clickNewButton();
	}
	
	@Test(priority = 10)
	public void fillingNewOpportunityForm() {
		editOpp.setOpportunityName(opportunityName);
		editOpp.setAccountName(accountName);
		editOpp.setCloseDate();
		editOpp.selectStage();
		editOpp.setMasterPricebook(masterPricebookName);
		editOpp.clickSavebutton();
	}
	
	@Test(priority = 20)
	public void setPartnerPricebook() throws InterruptedException {
		opp.clickChoosePricebook();
		Thread.sleep(1000);
		choosePri.selectPartnerPricebook(partnerPricebookName);
		Thread.sleep(1000);
		choosePri.clickSavebutton(wait);
		
		Thread.sleep(3000);
		Assert.assertTrue(opp.checkProductPricebook(partnerPricebookName));
		opp.clickConfigureLink();
	}
	
	@BeforeClass
	public void setup() {
		wait = new WebDriverWait(driver, 10);
		
		home = new HomePage(driver);
		homeOpp = new Home_Opportunities(driver);
		editOpp = new Edit_Opportunity(driver);
		opp = new Object_Opportunity(driver);
		choosePri = new ChoosePricebook(driver);
	}
}
