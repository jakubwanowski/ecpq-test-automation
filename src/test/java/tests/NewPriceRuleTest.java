package tests;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.Home_PriceRules;
import pages.New_PriceRule;
import pages.VisualForce_PriceRuleConfigurator;
import settings.Config;

public class NewPriceRuleTest extends Config {
	
	@Test(priority = 0)
	public void newPriceRulePreparation() throws InterruptedException {
		Thread.sleep(4000);
		
		home.clickAllTabsButton(wait);
		home.clickPriceRulesButton();
		
		homePriRuls.clickNewButton();
		newPriRule.clickContinueButton();
	}
	
	@Test(priority = 10)
	public void createPriceRule() throws InterruptedException {
		priRulsConfig.setPriceRuleName(priceRuleName);
		priRulsConfig.setRuleProdut(productName);
		
		priRulsConfig.addPriceRuleCondition();
		priRulsConfig.setPriceRuleConditionsAttribute(attributeName, wait);
		priRulsConfig.setPriceRuleConditionsOperator(wait);
		priRulsConfig.setPriceRuleConditionsValue(attributeVal1, wait);
		
		priRulsConfig.addPriceRuleAction();
		priRulsConfig.setPriceRuleActionsFieldName();
		priRulsConfig.setPriceRuleActionsActionType();
		priRulsConfig.setPriceRuleActionsTargetValue(adjustTargetValue);
		
		priRulsConfig.clickSaveButton(wait);
		priRulsConfig.clickSaveAndReturButton();
	}
	
	@BeforeClass
	public void setup() {
		wait = new WebDriverWait(driver, 10);
		wait60 = new WebDriverWait(driver, 60);
		
		home = new HomePage(driver);
		homePriRuls = new Home_PriceRules(driver);
		newPriRule = new New_PriceRule(driver);
		priRulsConfig = new VisualForce_PriceRuleConfigurator(driver);
	}

}
