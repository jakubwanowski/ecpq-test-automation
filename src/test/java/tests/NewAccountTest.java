package tests;

import pages.Edit_Account;
import pages.HomePage;
import pages.Home_Accounts;
import pages.Object_Account;
import settings.Config;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NewAccountTest extends Config {
	
	@Test(priority = 0)
	public void newAccountPreparation() throws InterruptedException {
		home.clickAccountsButton(wait);	
		homeAcc.clickNewButton();
		
		editAcc.waitForPageToLoad(wait);
		
		editAcc.setAccountName(accountName);
		editAcc.clickSaveButton();
		
		Assert.assertTrue(acc.getAccountNameInfo().contains(accountName));
	
	}
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		wait = new WebDriverWait(driver, 10);
		
		home = new HomePage(driver);
		homeAcc = new Home_Accounts(driver);
		editAcc = new Edit_Account(driver);
		acc = new Object_Account(driver);
	}
	

}
