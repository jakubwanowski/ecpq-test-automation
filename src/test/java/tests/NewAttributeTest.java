package tests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.Edit_Attribute;
import pages.Edit_AttributeValue;
import pages.HomePage;
import pages.Home_Attributes;
import pages.Object_Attribute;
import settings.Config;

public class NewAttributeTest extends Config {
	
	@Test(priority = 0)
	public void newAttributePreparation() throws InterruptedException {
		
		home.clickAttributesButton(wait);
		homeAtr.clickNewButton();
		
		editAtr.waitForPageToLoad(wait);
		editAtr.setAttributeName(attributeName);
		editAtr.selectAttributetype(attributeType);
		editAtr.clickSaveButton();
		
		Assert.assertEquals(atr.getAttributeNameInfo(), attributeName);
		Assert.assertEquals(atr.getAttributeTypeInfo(), attributeType);
	}
	
	@Test(priority = 10)
	public void addAttributeValues() throws InterruptedException {
		
		atr.clickNewAttributeValueButton();
		
		editAtrVal.setAttributeValueName(attributeVal1);
		editAtrVal.clicksaveAndNewButton();
		editAtrVal.waitForPageToLoad(wait);
		editAtrVal.setAttributeValueName(attributeVal2);
		editAtrVal.clickSaveButton();
	}
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		home = new HomePage(driver);
		homeAtr = new Home_Attributes(driver);
		editAtr = new Edit_Attribute(driver);
		atr = new Object_Attribute(driver);
		editAtrVal = new Edit_AttributeValue(driver);
	}

}
